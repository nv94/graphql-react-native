/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import LoginScreen from './src/component/LoginScreen';
import { createStackNavigator,createAppContainer } from 'react-navigation';
import { ApolloClient, InMemoryCache } from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { HttpLink } from 'apollo-link-http';


const client =  new ApolloClient({
  link: new HttpLink({ uri: 'https://app.bion.vn/api/graphql' }),
  cache : new InMemoryCache()
})
const RootStack = createStackNavigator(
  {
    Home: LoginScreen,
  },
  {
    initialRouteName: 'Home',
  }
);
const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return (
      <ApolloProvider client={client} >
        <AppContainer />
      </ApolloProvider>
    );
  }
}