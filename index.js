/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import LoginScreen from './src/component/LoginScreen';

AppRegistry.registerComponent(appName, () => App);
