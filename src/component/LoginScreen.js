import React, { Component } from 'react';
import { TextInput, StyleSheet, Text, View, Button } from 'react-native';
import gql from 'graphql-tag';
import { graphql, compose } from 'react-apollo';


const loginMutation = gql`
mutation ($organization_id: Int!, $email: String!, $password: String!) {
  login(organization_id: $organization_id, email: $email, password: $password) {
    token
  }
}
`
const registerMutation = gql`
mutation ($organization_id: Int!, $email: String!, $password: String!) {
  createPost(organization_id: $organization_id, email: $email, password: $password) {
    token
  }
}
`
class LoginScreen extends Component {
    constructor(props) {
        super(props);
        console.log(props);
    };

    static navigationOptions = {
        title: 'Bion',
        headerStyle: {
            backgroundColor: '#841584',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.viewTextInput}>
                    <Text>Tên tổ chức</Text>
                    <TextInput style={styles.textInput}
                        placeholder="Tên tổ chức"
                    ></TextInput>
                </View>
                <View style={styles.viewTextInput}>
                    <Text>Tên đăng nhập</Text>
                    <TextInput style={styles.textInput}
                        placeholder="Tên đăng nhập"
                    ></TextInput>
                </View>
                <View style={styles.viewTextInput}>
                    <Text>Mật khẩu</Text>
                    <TextInput style={styles.textInput}
                        secureTextEntry={true}
                        placeholder="Mật khẩu"
                    ></TextInput>
                </View>
                <View style={styles.bottonLogin}>
                    <Button
                        onPress={() => this.props.login(
                            {
                                organization_id: 1,
                                email: "thinhnv.58@gmail.com",
                                password: "Kploveafang@2",
                            }
                        )}
                        title="Đăng nhập"
                        color="#841584">
                    </Button>

                </View>
                <View style={styles.bottonLogin}>
                    <Button
                        
                        title="Đăng nhập"
                        color="#841584">
                    </Button>

                </View>
            </View>

        )
    }
}
export default compose(graphql(loginMutation, {
    props: ({ mutate }) => ({
        login: (input) =>
            mutate({
                variables: {
                    input,
                },
            }),
    })
}))(LoginScreen);


const styles = StyleSheet.create({
    container: {
        marginTop: 20
    },
    textInput: {
        height: 40, borderColor: '#841584', borderWidth: 1, borderRadius: 5,
        marginTop: 5, paddingLeft: 5,
    },
    viewTextInput: {
        marginLeft: 20, marginRight: 20, marginTop: 10
    },
    bottonLogin: {
        marginTop: 20, marginLeft: 20, marginRight: 20
    }
});

